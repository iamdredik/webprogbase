const mongoose = require("mongoose");
const config = require("./config");

module.exports = {
    async connect() {
        try {
            await mongoose.connect(config.dbUrl, config.connectOptions);
            console.log("DB listening at" + config.dbUrl);
        } catch (err) {
            console.log(err);
        }


    },
    async disconnect() {
        await mongoose.disconnect();
    }
};