require('dotenv').config({
    path: `${__dirname}/db.env`
});
const conf = {
    dbUrl: process.env.DB_HOST,
    connectOptions: {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false
    }
};
module.exports = conf;