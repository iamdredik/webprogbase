const gunController = require('../controllers/guns');
const gunRouter = require('express').Router();

gunRouter
    .get('/guns', gunController.getGuns)
    .get('/guns/newG', gunController.addPage)
    .get('/guns/:id', gunController.searchGunById)
    .post('/guns', gunController.deleteGunById, gunController.getGuns)
    .post('/guns/newG', gunController.addGun);

module.exports = gunRouter;