//

const userController = require('../controllers/users');
const userRouter = require('express').Router();

/**
 * @route GET /api/users
 * @group Users - user operations
 * @returns {Array.<User>} User - all users
 */

/**
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} limit.query - number of records per request
 * @param {integer} offset.query - number of skipped entries
 * @returns {Array.<User>} User - a page with users
 * @returns {Error} 400 - incorrect data query entered
 */

/**
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */

userRouter
    .get('/users', userController.getUsers)
    .get('/users/:id', userController.searchUserById);

module.exports = userRouter;