const express = require('express');
const router = express.Router();
const indexController = require('../controllers/index');
const userRouter = require('./users');
const tankRouter = require('./tanks');
const gunRouter = require('./guns');

router.get('/about', indexController.getAbout);
router.get('/', indexController.getHome);
router.use('', userRouter);
router.use('', tankRouter);
router.use('', gunRouter);
router.get('/favicon.ico', () => {

});





module.exports = router;