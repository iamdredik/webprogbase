//

const tankController = require('./../controllers/tanks');
const mediaController = require('./../controllers/media');
const tankRouter = require('express').Router();

tankRouter
    .get('/tanks', tankController.getTanks)
    .get('/tanks/new', tankController.addPage)
    .get('/tanks/:id', tankController.searchTankById)
    .post('/tanks', tankController.deleteTankById, tankController.getTanks)

    .post('/tanks/new', mediaController.addMedia, tankController.addTank);

module.exports = tankRouter;