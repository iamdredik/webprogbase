const routes = require('./routers/index');
const bodyParser = require('body-parser');
const express = require('express');
const busboy = require('busboy-body-parser');
const mustache = require('mustache-express');
const path = require('path');
const db = require("./db/db");
const app = express();
const port = process.env.PORT || 3000;

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);


const options = {
    swaggerDefinition: {
        info: {
            description: 'JSON HTTP API a web server that provides access to storage resources',
            title: 'lab 2',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: ["application/json"],
    },
    basedir: __dirname,
    files: ['./routers/**/*.js', './models/**/*.js'],
};
expressSwagger(options);



const optionsbbp = {
    limit: '5mb',
    multi: false,
};
app.use(busboy(optionsbbp));




app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use('/', routes);

const morgan = require('morgan');
app.use(morgan('dev'));

app.use(express.static('public'));

const viewsDir = path.join(__dirname, 'views');

app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use((err) => {
    console.log(`On error: ${err.message} ${err}`);
    // console.dir(err);
});




app.listen(port, async () => {


    db.connect();
    console.log(`Example app listening at http://localhost:${port}`);
});