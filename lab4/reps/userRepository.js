// const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
const User = require("../models/user");

class UserRepository {

    constructor(filePath) {
        // this.storage = new JsonStorage(filePath);
    }

    async getUsers() {
        // console.dir(await User.findOne().populate("tanks").map(q => q.toJSON()));
        return await User.find();
    }

    async getUserById(id) {
        // const items = this.storage.readItems();
        // for (const item of items.items) {
        //     if (parseInt(item.id) === parseInt(id)) {
        //         return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
        //     }
        // }
        return await User.findById(id);
    }
};

module.exports = UserRepository;