
const Gun = require("../models/gun");

class GunRepository {

    constructor(filePath) {
        // this.storage = new JsonStorage(filePath);
    }

    async addGun(newGun) {
        try {

            const newT = await new Gun(newGun).save();
            return newT._id;
        } catch (err) {
            console.log(err);
        }
    }

    async getGuns() {
        try {
            return await Gun.find();
        } catch (err) {
            console.log(err);
        }
    }

    async getGunById(id) {
        try {
            return await Gun.findOne({
                _id: id
            });
        } catch (err) {
            console.log(err);
        }
    }

    async deleteGun(id) {
        try {
            await Gun.deleteOne({
                _id: id
            });
        } catch (err) {
            console.log(err);
        }
    }
};

module.exports = GunRepository;