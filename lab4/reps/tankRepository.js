
const Tank = require("../models/tank");

class TankRepository {

    constructor(filePath) {
        // this.storage = new JsonStorage(filePath);
    }

    async addTank(newTank) {
        try {

            const newT = await new Tank(newTank).save();
            return newT._id;
        } catch (err) {
            console.log(err);
        }
    }

    async getTanks() {
        try {
            return await Tank.find();
        } catch (err) {
            console.log(err);
        }
    }

    async getTankById(id) {
        try {
            return await Tank.findOne({
                _id: id
            });
        } catch (err) {
            console.log(err);
        }
    }

    updateTank(updatedTank) {
        const items = this.storage.readItems();
        for (const item of items.items) {
            if (parseInt(item.id) === parseInt(updatedTank.id)) {
                item.name = updatedTank.name;
                item.nation = updatedTank.nation;
                item.tier = parseInt(updatedTank.tier);
                item.damage = parseInt(updatedTank.damage);
                item.dateOfCreate = updatedTank.dateOfCreate;
                item.img = updatedTank.img;
                this.storage.writeItems(items);
                return;
            }
        }
        console.log("Tank was not found!".red);
        return null;
    }

    async deleteTank(id) {
        try {
            await Tank.deleteOne({
                _id: id
            });
        } catch (err) {
            console.log(err);
        }
    }
};

module.exports = TankRepository;