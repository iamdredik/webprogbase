//

const Media = require('./../models/media');
const JsonStorage = require('../jsonStorage');
const fs = require('fs');



class MediaRepository {

    constructor(path) {}

    async addMedia(req, res) {
        
    }

    getMediaById(id) {
        const medias = this.storage.readItems();
        for (const media of medias.items) {
            if (media.name === id) {
                return new Media(id, media.name, media.encoding, media.mimetype, media.truncated, media.url, media.size);
            }
        }
        return null;
    }

};

module.exports = MediaRepository;