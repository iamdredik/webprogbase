const mongoose = require("mongoose");

const GunSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    rateOfFire: {
        type: Number
    },
    penetration: {
        type: Number
    },
    damage: {
        type: Number
    },
    tanks: [{
        type: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'tank'
        }
    }]

});

module.exports = mongoose.model('gun', GunSchema);