const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
    login: {
        type: String,
        required: true
    },
    fullname: {
        type: String
    },
    registeredAt: {
        type: Date,
        default: Date.now
    },
    role: {
        type: Number
    },
    avaUrl: {
        type: String
    },
    isEnabled: {
        type: Boolean
    },
    tanks: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'tank'
        }]
    }

});

module.exports = mongoose.model('user', UserSchema);