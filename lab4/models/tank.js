const mongoose = require("mongoose");

const TankSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    nation: {
        type: String
    },
    dateOfCreate: {
        type: Date,
        default: Date.now
    },
    tier: {
        type: Number
    },
    img: {
        type: String
    },
    owner: {
        type: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
        }
    },
    gun: {
        type: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'gun'
        }
    }

});

module.exports = mongoose.model('tank', TankSchema);