//

const Media = require('./../models/media');
const mediaRepository = require('./../reps/mediaRepository');
const storage = new mediaRepository('./data/media.json');

const searchMedia = Symbol('media');

const cloudinary = require('cloudinary');
const { format } = require('morgan');
cloudinary.config({
    cloud_name: 'dtashgcgp',
    api_key: 826778294358921,
    api_secret: '-qloV3Mrc0yg2MNE49PwrkvXRaU'
});

async function uploadRaw(buffer, req) {
    return new Promise((resolve, reject) => {

        cloudinary.v2.uploader.upload_stream({
                    resource_type: 'raw',
                    format: 'jpg',
                    resource_type: 'image'
                },
                function (error, result) {
                    req.imgUrl = result.url;
                    resolve(result);
                })
            .end(buffer);
    });
}


module.exports = {

    async addMedia(req, res, next) {
        if (req.files.imgFile) {
            // const media = new Media(1, req.files.imgFile.name, req.files.imgFile.encoding, req.files.imgFile.mimetype, req.files.imgFile.truncated, "", req.files.imgFile.size);
            const fileBuffer = req.files.imgFile.data;
            await uploadRaw(fileBuffer, req);
        }
        next();

    },

    getMediaById(req, res) {
        res.statusCode = 200;
        res.download(req[searchMedia].url);
    },

    searchMediaById(req, res, next) {
        const idMedia = req.params.name;
        console.log(req.params.name);
        const media = storage.getMediaById(idMedia);
        if (media) {
            req[searchMedia] = media;
            next();
        } else {
            res.sendStatus(404);
        }
    }

};