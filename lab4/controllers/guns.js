const gunRepository = require('./../reps/gunRepository');
const storage = new gunRepository('./data/guns.json');
const Gun = require('./../models/gun');

module.exports = {

    async getGuns(req, res) {
        const limit = 9;
        const gun = await storage.getGuns();
        if (req.query.search) {
            const search = req.query.search;

            for (let i = 0; i < gun.length;) {
                if (!gun[i].name.includes(search)) {
                    gun.splice(i, 1);

                } else {
                    i++;
                }
            }
        }

        const offset = parseInt(req.query.offset);
        if (limit < 0 || offset < 0) {
            res.render('guns', {
                guns: true,
                entities: 0,
                pagination: await getInfo(req, res)

            });
        } else {
            const gunsLimit = gun.slice(offset, offset + limit);
            res.render('guns', {
                guns: true,
                entities: gunsLimit,
                pagination: await getInfo(req, res)

            });
        }

    },

    async addGun(req, res) {

        const gun = new Gun(null, null, null, null);
        gun.damage = req.body.gunDamage ? parseFloat(req.body.gunDamage) : gun.gunDamage;
        gun.penetration = req.body.gunPen ? parseFloat(req.body.gunPen) : gun.gunPen;
        gun.rateOfFire = req.body.gunRate;
        gun.name = req.body.gunName;

        gun.id = await storage.addGun(gun);
        console.log(gun);
        res.redirect('/guns/' + gun.id);

    },

    async deleteGunById(req, res, next) {
        console.log(`Request: ${req.method} /api/guns${req.body.id}`);
        await storage.deleteGun(req.body.id);
        next();
    },

    async searchGunById(req, res) {
        const gun = await storage.getGunById(req.params.id);
        res.render('gun', {
            gun: gun,
        });
    },
    async addPage(req, res) {
        res.render('newG');
    }


};

async function getInfo(req, res) {
    const p = {
        first: 0,
        last: 0,
        prev: 0,
        next: 0,
        firstActive: true,
        lastActive: true,
        prevActive: true,
        nextActive: true,
        search: "",
        notfound: false,
        ifsearch: false,
        currPage: 1,
        maxPage: 1
    };
    const guns = await storage.getGuns();
    if (req.query.search) {
        p.ifsearch = true;
        p.search = req.query.search;
        const search = req.query.search;
        for (let i = 0; i < guns.length;) {
            if (!guns[i].name.includes(search)) {
                guns.splice(i, 1);

            } else {
                i++;
            }
        }
    }
    p.maxPage = Math.ceil(guns.length / 9);
    p.currPage = parseInt(req.query.offset) / 9 + 1;
    p.last = (Math.ceil(guns.length / 9) - 1) * 9;
    if (parseInt(req.query.offset) === p.last || p.last < 0) {
        p.lastActive = false;
        p.nextActive = false;
    }
    if (p.last < 0) {
        p.notfound = true;
        p.maxPage = 1;
    }
    if (parseInt(req.query.offset) === 0) {
        p.prevActive = false;
        p.firstActive = false;
    }
    p.next = parseInt(req.query.offset) + 9;
    p.prev = (Math.ceil(parseInt(req.query.offset) / 9) - 1) * 9;
    return p;
}