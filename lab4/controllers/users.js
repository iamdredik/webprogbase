const userRepository = require('./../reps/userRepository');
const storage = new userRepository('./data/users.json');

module.exports = {

    async getUsers(req, res) {
        const limit = 9;
        const users = await storage.getUsers();
        
        const offset = parseInt(req.query.offset);
        if (limit < 0 || offset < 0) {
            res.render('users', {
                users: true,
                entities: 0,
                pagination: await getInfo(req, res)
            });
        } else {
            const usersLimit = users.slice(offset, offset + limit);
            res.render('users', {
                users: true,
                entities: usersLimit,
                pagination: await getInfo(req, res)
            });
        }
    },

    async searchUserById(req, res) {
        const user = await storage.getUserById(req.params.id);
        res.render('user', {
            user: user
        });

    }
};

async function getInfo(req, res) {
    const p = {
        first: 0,
        last: 0,
        prev: 0,
        next: 0,
        firstActive: true,
        lastActive: true,
        prevActive: true,
        nextActive: true,
        search: "",
        notfound: false,
        ifsearch: false,
        currPage: 1,
        maxPage: 1
    };
    const users = await storage.getUsers();
    if (req.query.search) {
        p.ifsearch = true;
        p.search = req.query.search;
        const search = req.query.search;
        for (let i = 0; i < users.length;) {
            if (!users[i].name.includes(search)) {
                users.splice(i, 1);

            } else {
                i++;
            }
        }
    }
    p.maxPage = Math.ceil(users.length / 9);
    p.currPage = parseInt(req.query.offset) / 9 + 1;
    p.last = (Math.ceil(users.length / 9) - 1) * 9;
    if (parseInt(req.query.offset) === p.last || p.last < 0) {
        p.lastActive = false;
        p.nextActive = false;
    }
    if (p.last < 0) {
        p.notfound = true;
        p.maxPage = 1;
    }
    if (parseInt(req.query.offset) === 0) {
        p.prevActive = false;
        p.firstActive = false;
    }
    p.next = parseInt(req.query.offset) + 9;
    p.prev = (Math.ceil(parseInt(req.query.offset) / 9) - 1) * 9;
    return p;
    // if (tanks.length / 7)
}