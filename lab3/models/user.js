/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - username
 * @property {string} fullname.required - full username
 * @property {integer} role - user status
 * @property {string} registeredAt - registration date
 * @property {string} avaUrl - link to avatar
 * @property {bool} isEnable - state of activity
 */

 class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isEnabled = isEnabled;
    }
};

module.exports = User;