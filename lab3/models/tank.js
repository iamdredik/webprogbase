/**
 * @typedef Tank
 * @property {integer} id
 * @property {string} name - tank name
 * @property {string} nation - nation name
 * @property {string} tier - tier of tank
 * @property {integer} damage - tank characteristic
 * @property {string} dateOfCreate - tank registration date
 */

 class Tank {

    constructor(id, name, nation, tier, damage, dateOfCreate, img) {
        this.id = id; // number
        this.name = name; // string
        this.nation = nation; // string
        this.tier = tier; //int
        this.damage = damage; //int
        this.dateOfCreate = dateOfCreate;
        this.img = img;
    }
};

module.exports = Tank;