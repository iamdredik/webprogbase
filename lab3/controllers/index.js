module.exports = {
    getHome(req, res) {
        res.render('index', {
            home: true,
            title: 'home'
        });
    },
    getAbout(req, res) {
        res.render('about', {
            about: true,
            title: 'about'
        });
    }
};