//

const Media = require('./../models/media');
const mediaRepository = require('./../reps/mediaRepository');
const storage = new mediaRepository('./data/media.json');

const searchMedia = Symbol('media');

module.exports = {

    addMedia(req, res, next) {
        const media = new Media(1, req.files.imgFile.name, req.files.imgFile.encoding, req.files.imgFile.mimetype, req.files.imgFile.truncated, "", req.files.imgFile.size);
        storage.addMedia(media, req.files.imgFile.data);
        next();

    },

    getMediaById(req, res) {
        res.statusCode = 200;
        res.download(req[searchMedia].url);
    },

    searchMediaById(req, res, next) {
        const idMedia = req.params.name;
        console.log(req.params.name);
        const media = storage.getMediaById(idMedia);
        if (media) {
            req[searchMedia] = media;
            next();
        } else {
            res.sendStatus(404);
        }
    }

};