const tankRepository = require('./../reps/tankRepository');
const storage = new tankRepository('./data/tanks.json');
const Tank = require('./../models/tank');

module.exports = {

    getTanks(req, res) {
        const limit = 9;
        const tank = storage.getTanks();
        if (req.query.search) {
            const search = req.query.search;

            for (let i = 0; i < tank.length;) {
                if (!tank[i].name.includes(search)) {
                    tank.splice(i, 1);

                } else {
                    i++;
                }
            }
        }

        const offset = parseInt(req.query.offset);
        if (limit < 0 || offset < 0) {} else if (tank.length <= offset) {
            return 0;
        } else {
            const tanksLimit = tank.slice(offset, offset + limit);
            res.render('tanks', {
                tanks: true,
                entities: tanksLimit,
                pagination: getInfo(req, res)

            });
        }

    },

    addTank(req, res) {

        const tank = new Tank(null, null, null, null, null, null, null);
        // 
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        // 
        tank.dateOfCreate = req.body.dateOfCreate ? Date.parse(req.body.dateOfCreate) : today;
        tank.damage = req.body.tankDamage ? parseFloat(req.body.tankDamage) : tank.tankDamage;
        tank.tier = req.body.tankTier ? parseFloat(req.body.tankTier) : tank.tankTier;
        tank.nation = req.body.tankNation;
        tank.name = req.body.tankName;
        if (req.files.imgFile) tank.img = req.files.imgFile.name;
        tank.id = storage.addTank(tank);

        res.redirect('/tanks/' + tank.id);

    },

    updataTank(req, res) {
        console.log(`Request: ${req.method} /api/tanks${req.url}?options`);
        if (req.body.id) {
            const tankId = parseInt(req.body.id);
            const tank = storage.getTankById(tankId);
            if (tank) {
                tank.name = req.body.name ? req.body.name : tank.name;
                tank.nation = req.body.nation ? req.body.nation : tank.nation;
                // 
                let today = new Date();
                let dd = String(today.getDate()).padStart(2, '0');
                let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                let yyyy = today.getFullYear();

                today = mm + '/' + dd + '/' + yyyy;
                // 
                tank.dateOfCreate = req.body.dateOfCreate ? Date.parse(req.body.dateOfCreate) : today;
                tank.tier = req.body.tier ? parseFloat(req.body.tier) : tank.tier;
                tank.damage = req.body.damage ? parseFloat(req.body.damage) : tank.damage;
                storage.updateTank(tank);

                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/json');
                const tankOut = JSON.stringify(tank, null, 4);
                res.end(tankOut);
            } else {
                res.sendStatus(404);
            }
        } else {
            res.sendStatus(400);
        }
    },

    deleteTankById(req, res, next) {
        console.log(`Request: ${req.method} /api/tanks${req.body.id}`);
        storage.deleteTank(req.body.id);
        next();
    },

    searchTankById(req, res) {
        const tank = storage.getTankById(req.params.id);
        res.render('tank', {
            tank: tank,
        });
    },
    addPage(req, res) {
        res.render('new');
    }


};

function getInfo(req, res) {
    const p = {
        first: 0,
        last: 0,
        prev: 0,
        next: 0,
        firstActive: true,
        lastActive: true,
        prevActive: true,
        nextActive: true,
        search: "",
        notfound: false,
        ifsearch: false,
        currPage: 1,
        maxPage: 1
    };
    const tanks = storage.getTanks();
    if (req.query.search) {
        p.ifsearch = true;
        p.search = req.query.search;
        const search = req.query.search;
        for (let i = 0; i < tanks.length;) {
            if (!tanks[i].name.includes(search)) {
                tanks.splice(i, 1);

            } else {
                i++;
            }
        }
    }
    p.maxPage = Math.ceil(tanks.length / 9);
    p.currPage = parseInt(req.query.offset) / 9 + 1;
    p.last = (Math.ceil(tanks.length / 9) - 1) * 9;
    if (parseInt(req.query.offset) === p.last || p.last < 0) {
        p.lastActive = false;
        p.nextActive = false;
    }
    if (p.last < 0) {
        p.notfound = true;
        p.maxPage = 1;
    }
    if (parseInt(req.query.offset) === 0) {
        p.prevActive = false;
        p.firstActive = false;
    }
    p.next = parseInt(req.query.offset) + 9;
    p.prev = (Math.ceil(parseInt(req.query.offset) / 9) - 1) * 9;
    return p;
    // if (tanks.length / 7)
}