const fs = require('fs');

class JsonStorage {

    // filePath - path to JSON file
    constructor(filepath) {
        this.filepath = filepath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.nextId;
    }

    incrementNextId() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        jsonArray.nextId++;
        this.writeItems(jsonArray);
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    writeItems(items) {
            const temp = JSON.stringify(items, null, '\t');
            fs.writeFileSync(this.filepath, temp,);
    }
};

module.exports = JsonStorage;