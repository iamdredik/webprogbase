//

const Media = require('./../models/media');
const JsonStorage = require('../jsonStorage');
const fs = require('fs');

class MediaRepository {

    constructor(path) {
        this.storage = new JsonStorage(path);
    }

    addMedia(media, data) {
        const medias = this.storage.readItems();
        const id = this.storage.nextId;
        const pathFile = `./data/media/${media.name}`;
        fs.writeFileSync(pathFile, data);
        media.id = id;
        media.url = pathFile;
        medias.items.push(media);
        this.storage.writeItems(medias);
        this.storage.incrementNextId();
        return id;
    }

    getMediaById(id) {
        const medias = this.storage.readItems();
        for (const media of medias.items) {
            if (media.name === id) {
                return new Media(id, media.name, media.encoding, media.mimetype, media.truncated, media.url, media.size);
            }
        }
        return null;
    }

};

module.exports = MediaRepository;