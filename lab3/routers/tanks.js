//

const tankController = require('./../controllers/tanks');
const mediaController = require('./../controllers/media');
const tankRouter = require('express').Router();

/**
 * @route GET /api/tanks
 * @group Tank - tanks operations
 * @returns {Array.<Tank>} Tabks - all tanks
 */

/**
 * @route GET /api/tanks?limit={x}&offset={y}
 * @group Tank - tanks operations
 * @param {integer} limit.query - number of tanks per request
 * @param {integer} offset.query - number of skipped entries
 * @returns {Array.<Tank>} Tanks - a page with tanks
 * @returns {Error} 400 - incorrect data query entered
 */

/**
 * @route GET /api/tanks/{id}
 * @group Tank - tanks operations
 * @param {integer} id.path.required - id of the Tank
 * @returns {Tank.model} 200 - tank object
 * @returns {Error} 404 - tank not found
 */

/**
 * @route POST /api/tanks
 * @group Tank - tank operations
 * @param {Tank.model} name.body.required - name
 * @param {Tank.model} nation.body.required - name of nation
 * @param {Tank.model} dateOfCreate.body - tank release date
 * @param {Tank.model} tier.body - tank duration
 * @param {Tank.model} damage.body - tank popularity
 * @returns {Tank.model} 201 - new tank object
 * @returns {Error} 400 - required parameters are not specified
 */

/**
 * @route PUT /api/tanks
 * @group Tank - tank operations
 * @param {Tank.model} id.body.required - tank id
 * @param {Tank.model} name.body.required - name
 * @param {Tank.model} nation.body.required - name of nation
 * @param {Tank.model} dateOfCreate.body - tank release date
 * @param {Tank.model} tier.body - tank duration
 * @param {Tank.model} damage.body - tank popularity
 * @returns {Tank.model} 200 - updated tank object
 * @returns {Error} 404 - tank not found
 * @returns {Error} 400 - if the required id parameter is not specified
 */

/**
 * @route DELETE /api/tanks/{id}
 * @group Tank - tank operations
 * @param {integer} id.path.required - id of the tank
 * @returns {Tank.model} 200 - deleted tank object
 * @returns {Error} 404 - tank not found
 */

tankRouter
    .get('/tanks', tankController.getTanks)
    .get('/tanks/:id(\\d+)', tankController.searchTankById)
    .post('/tanks', tankController.deleteTankById, tankController.getTanks)
    .get('/tanks/new', tankController.addPage)
    .post('/tanks/new', mediaController.addMedia, tankController.addTank);

module.exports = tankRouter;