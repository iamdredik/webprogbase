const express = require('express');
const router = express.Router();
const indexController = require('../controllers/index');
const userRouter = require('./users');
const tankRouter = require('./tanks');

router.get('/about', indexController.getAbout);
router.get('/', indexController.getHome);
router.use('', userRouter);
router.use('', tankRouter);
router.get('/favicon.ico', () => {
    
});





module.exports = router;