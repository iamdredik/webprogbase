const User = require('./../models/user');
const JsonStorage = require('./../jsonStorage');
class UserRepository {
 
    constructor(filepath) {
        this.jsonSt = new JsonStorage(filepath);
        this.jsonSt.readItems();
        const jsonArray = Array.from(this.jsonSt.readItems());
        this.userArray = [];
        for (let index = 0; index < jsonArray.length; index++) {
            let user = new User(jsonArray[index].id, jsonArray[index].login, jsonArray[index].fullname, jsonArray[index].role, jsonArray[index].registeredAt, jsonArray[index].avaUrl, jsonArray[index].isEnabled);
            this.userArray.push(user);
        }
    }
 
    addUser(newUser) {
        newUser.id = this.jsonSt.nextId;
        this.userArray.push(newUser);
        this.jsonSt.incrementNextId();
        this.jsonSt.writeItems(this.userArray);
    }

    getUsers() {
        return this.userArray;
    }
 
    getUserById(id) {
        if ((this.userArray.some(x => x.id === id)) === true){
            return this.userArray.find(elem => elem.id === id);
        }
        else {
            console.log(`Error: user with id ${id} not found.`);
            return;
        }
    }
};
 
module.exports  = UserRepository;
