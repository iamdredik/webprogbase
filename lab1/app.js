const readline = require('readline-sync');

const UserRepository = require('./repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');
const User = require('./models/user');

const AnimeRepository = require('./repositories/animeRepository');
const animeRepository = new AnimeRepository("./data/anime.json");
const Anime = require('./models/anime');



while(true) {
    const input = readline.question("Enter your command: ");
    const text = input.toString().trim();
    const parts = text.split("/");
    const command = parts[0];
    const entity = parts[1];
    if (command === "get") {
        if (entity === "users") {
            console.log(userRepository.getUsers());
        }
        else if (entity === "user") {
            const id = parts[2];
            console.log(userRepository.getUserById(parseInt(id)));
        }
        else if (entity === "animes") {
            console.log(animeRepository.getAnimes());
        }
        else if (entity === "anime") {
            const id = parts[2];
            console.log(animeRepository.getAnimeById(parseInt(id)));
        }
        else {
            console.log(`Not supported entity: '${entity}'`);
        }
    } 
    else if (command === "delete") {
        const id = parts[2];
        if(entity === "animes") {
            const animeId = parseInt(id);
            animeRepository.deleteAnime(animeId);
        }
        else {
            console.log(`Not supported entity: '${entity}'`);
        }
    } 
    else if(command === "update") {
        const id = parts[2];
        if(entity === "animes") {
            const animeId = parseInt(id);
            if (animeRepository.getAnimeById(animeId) !== -1) {
                const updAnime = new Anime();
                updAnime.id = animeId;
                updAnime.title = readline.question("Enter title: ");
                updAnime.studio = readline.question("Enter studio: ");
                updAnime.ongoing = readline.question("Enter state(0 - released, 1 - ongoing): ");
                updAnime.releaseDate = readline.question("Enter date of release in ISO-format: ");
                updAnime.episodes = readline.question("Enter amount of episodes: ");
                console.log(updAnime);
                animeRepository.updateAnime(updAnime);
            }
        }
        else {
            console.log(`Not supported entity: '${entity}'`);
        }
    }
    else if(command === "post") {
        if (entity === "users") {
            let addUser = new User();
            addUser.id = "";
            addUser.login = readline.question("Enter login: ");
            addUser.fullname = readline.question("Enter fullname: ");
            addUser.role = readline.question("Enter role(0 - guest, 1 - admin): ");
            addUser.registeredAt = readline.question("Enter date of registration in ISO-format: ");
            addUser.avaUrl = readline.question("Enter URL: ");
            addUser.isEnabled = readline.question("Enter state(0 - disabled, 1 - enabled): ");
            userRepository.addUser(addUser);
        }
        else if (entity === "animes") {
            let addAnime = new Anime();
            addAnime.id = "";
            addAnime.title = readline.question("Enter title: ");
            addAnime.studio = readline.question("Enter studio: ");
            addAnime.ongoing = readline.question("Enter state(0 - released, 1 - ongoing): ");
            addAnime.releaseDate = readline.question("Enter date of release in ISO-format: ");
            addAnime.episodes = readline.question("Enter amount of episodes: ");
            animeRepository.addAnime(addAnime);
        }
        else {
            console.log(`Not supported entity: '${entity}'`);
        }
    }
    else if (command === "quit") {
        break;
    }
    else {
        console.log(`Not supported command: '${command}'`);
    }
}
