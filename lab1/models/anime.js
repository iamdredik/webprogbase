class Anime {

    constructor(id, title, studio, ongoing, releaseDate, episodes) {
        this.id = id;
        this.title = title;
        this.studio = studio;
        this.ongoing = ongoing;
        this.releaseDate = releaseDate;
        this.episodes = episodes;
   }
};

module.exports = Anime;