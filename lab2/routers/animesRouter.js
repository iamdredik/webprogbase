const animeRouter = require('express').Router();
const animeController = require('../controllers/animesController');

/**
 * Receiveng all anime
 * @route GET /api/animes
 * @group Anime - anime operations
 * @returns {Array.<Anime>} Anime - all anime
*/

/**
 * Receiving anime with pagination
 * @route GET /api/animes
 * @group Anime - anime operations
 * @param {integer} limit.query - number of titles on page
 * @param {integer} page.query - number of page
 * @returns {Array.<Anime>} Anime - a page with anime
 * @returns {Error} 400 - incorrect data query entered
*/

/**
 * Receiving an anime by id
 * @route GET /api/animes/{id}
 * @group Anime - anime operations
 * @param {integer} id.path.required - id of the Anime
 * @returns {Anime.model} 200 - anime object
 * @returns {Error} 404 - anime not found
*/

/**
 * Recording a new anime to the server
 * @route POST /api/animes
 * @group Anime - anime operations
 * @param {Anime.model} title.body.required - name of anime
 * @param {Anime.model} studio.body - name of studio
 * @param {Anime.model} ongoing.body - is it already released or not
 * @param {Anime.model} releaseDate.body - date of release
 * @param {Anime.model} episodes.body - amount of episodes
 * @returns {Anime.model} 201 - new Anime object
 * @returns {Error} 400 - required parameters are not specified
*/

/**
 * Updating anime data on the server
 * @route PUT /api/animes
 * @group Anime - anime operations
 * @param {Anime.model} id.body.required - anime id
 * @param {Anime.model} title.body - name of anime
 * @param {Anime.model} studio.body - name of studio
 * @param {Anime.model} ongoing.body - is it already released or not
 * @param {Anime.model} releaseDate.body - date of release
 * @param {Anime.model} episodes.body - amount of episodes
 * @returns {Anime.model} 200 - updated Anime object
 * @returns {Error} 404 - anime not found
 * @returns {Error} 400 - if the required id parameter is not specified
*/

/**
 * Delete anime by id
 * @route DELETE /api/animes/{id}
 * @group Anime - anime operations
 * @param {integer} id.path.required - id of the Anime
 * @returns {Anime.model} 200 - deleted Anime object
 * @returns {Error} 404 - anime not found
*/

animeRouter
    .get("/:id(\\d+)", animeController.getAnimeById)
    .get("/", animeController.getAnimes)
    .post("/", animeController.postAnime)
    .put("/", animeController.putAnime)
    .delete("/:id(\\d+)", animeController.deleteAnime);
module.exports = animeRouter;