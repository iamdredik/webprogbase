
const userRouter = require('express').Router();
const userController = require('../controllers/usersController');

/**
 * Getting all users
 * @route GET /api/users
 * @group Users - user operations
 * @returns {Array.<User>} User - all users
*/

/**
 * TODO: Receiving users with pagination.
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} limit.query - number of entities on page
 * @param {integer} page.query - number of page
 * @returns {Array.<User>} User - a page with users
 * @returns {Error} 400 - incorrect data query entered
*/

/**
 * Getting a user by id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
*/

userRouter
    .get("/:id(\\d+)", userController.getUserById)
    .get("/", userController.getUsers);

module.exports = userRouter;