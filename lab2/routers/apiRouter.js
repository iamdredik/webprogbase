const userRouter = require('./usersRouter');
const animeRouter = require('./animesRouter');
const mediaRouter = require('./mediaRouter');

const apiRouter = require('express').Router();

apiRouter
    .use('/users', userRouter)
    .use('/animes', animeRouter)
    .use('/media', mediaRouter);

module.exports = apiRouter;