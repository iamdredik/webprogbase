const express = require('express');
const bodyParser = require('body-parser');
const busboy = require('busboy-body-parser');
const expressSwaggerGenerator = require('express-swagger-generator');

const router = require('./routers/apiRouter');


const port = 3015;
const app = express();

const optionsBusboy = {
	limit: '5mb',
	multi: false,
};

const expressSwagger = expressSwaggerGenerator(app);
const optionsExpressSwagger = {
    swaggerDefinition: {
        info: {
            description: 'JSON HTTP API Server for my favorite titles',
            title: 'JSON HTTP API Anime, Users and Images Server',
            version: '1.0.0',
        },
        host: 'localhost:3015',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routers/**/*.js', './models/**/*.js'],
};

app.use(busboy(optionsBusboy));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api', router);
app.use((err, req, res, next) => { console.log(`On error: ${err.message}`); });
expressSwagger(optionsExpressSwagger);
app.listen(port, () => { console.log(`The server is listening at http://localhost:${port}`); });