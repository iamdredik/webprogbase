/**
 * @typedef Anime
 * @property {integer} id
 * @property {string} title.required - name of anime
 * @property {string} studio - name of studio
 * @property {integer} ongoing - is it already released or not
 * @property {string} releaseDate - date of release
 * @property {integer} episodes - amount of episodes
 */
class Anime {

    constructor(id, title, studio, ongoing, releaseDate, episodes) {
        this.id = id;
        this.title = title;
        this.studio = studio;
        this.ongoing = ongoing;
        this.releaseDate = releaseDate;
        this.episodes = episodes;
   }
};

module.exports = Anime;