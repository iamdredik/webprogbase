/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - user`s fullname
 * @property {integer} role - user`s role(0 - guest, 1 - admin)
 * @property {string} registeredAt - when this account was created
 * @property {string} avaUrl - url-link to avatar image
 * @property {bool} isEnabled - does this user active - 1 or not - 0
 */

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;
        this.login = login;
        this.fullname = fullname;
        this.role = role;
        this.registeredAt = registeredAt;
        this.avaUrl = avaUrl;
        this.isEnabled = isEnabled;
    }
};

module.exports = User;