/**
 * @typedef Media
 * @property {integer} id
 * @property {string} name - file name
 * @property {string} encoding - encoding type
 * @property {string} mimetype - file type
 * @property {bool} truncated - modification
 * @property {string} url - link to the file on the server
 * @property {integer} size - file size
 */

class Media
{
	constructor(id, name, encoding, mimetype, truncated, url, size) {
		this.id = id;
		this.name = name;
		this.encoding = encoding;
		this.mimetype = mimetype;
		this.truncated = truncated;
		this.url = url;
		this.size = size;
	}
};

module.exports = Media;