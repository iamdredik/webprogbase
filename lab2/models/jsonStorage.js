const fs = require('fs');

class JsonStorage {
    constructor(filepath){
        this.filePath = filepath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.nextId;
    }

    incrementNextId() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        ++jsonArray.nextId;
        const newObj = {
            nextId: jsonArray.nextId,
            items: jsonArray.items
        };
        const jsonItems = JSON.stringify(newObj, null, 4);
        fs.writeFileSync(this.filePath, jsonItems);
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.items;
    }

    writeItems(items) {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        const newObj = {
            nextId: jsonArray.nextId,
            items: items
        };
        const jsonItems = JSON.stringify(newObj, null, 4);
        fs.writeFileSync(this.filePath, jsonItems);
    }
};

module.exports = JsonStorage;