const Anime = require('./../models/anime');
const JsonStorage = require('./../models/jsonStorage');
 
class AnimeRepository {
 
    constructor(filepath) {
        this.filepath = filepath.toString();
        this.jsonSt = new JsonStorage(filepath);
        const jsonArray = Array.from(this.jsonSt.readItems());
        this.animeArray = [];
        for (let index = 0; index < jsonArray.length; index++) {
            let anime = new Anime(jsonArray[index].id, jsonArray[index].title, jsonArray[index].studio, jsonArray[index].ongoing, jsonArray[index].releaseDate, jsonArray[index].episodes);
            this.animeArray.push(anime);
        }
    }
 
    addAnime(newAnime) {
        newAnime.id = this.jsonSt.nextId;
        this.animeArray.push(newAnime);
        this.jsonSt.incrementNextId();
        this.jsonSt.writeItems(this.animeArray);
        return newAnime;
    }

    getAnimes() {
        return this.animeArray; 
    }
 
    getAnimeById(id) {
        if ((this.animeArray.some(x => x.id === id)) === true){
            return this.animeArray.find(elem => elem.id === id);
        }
        else {
            console.log(`Error: anime with id ${id} not found.`);
            return null;
        }
    }

    updateAnime(newAnime) {
            const ind = this.animeArray.indexOf(this.animeArray.find(x => x.id === newAnime.id));
            this.animeArray[ind] = newAnime;
            this.jsonSt.writeItems(this.animeArray);
            return newAnime;
    }

    deleteAnime(id) {
        if ((this.animeArray.some(x => x.id === id)) === true){
            const deleted = this.animeArray.find(x => x.id === id);
            this.animeArray.splice(this.animeArray.indexOf(deleted), 1);
            this.jsonSt.writeItems(this.animeArray);
            return deleted;
        }
        else {
            console.log(`Error: anime with id ${id} not found.`);
            return null;
        }
    }
};
 
module.exports  = AnimeRepository;