const UserRepository = require('../repositories/userRepository');
const useRep = new UserRepository('./data/users.json');

module.exports = {
    getUsers(req, res) {
        console.log(`Your request: ${req.metod} /api/users${req.url}`);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json');
        const users = useRep.getUsers();
        if (req.query.limit) {
            const limit = parseFloat(req.query.limit);
            let arrLen = users.length;
            let pages = 0;
            if (arrLen < limit) {
                res.sendStatus(404);
            }
            for (let i = arrLen; i > 0; i -= limit, ++pages) {}
            const page = parseInt(req.query.page);
            if (page <= pages) {
                const ind = (page - 1) * limit;
                let iter = 0;
                for (; iter < limit; iter++) {
                    if (ind + iter === arrLen) {
                        break;
                    }
                }
                const usersLimit = users.slice(ind, ind + iter);
                const jsonText = JSON.stringify(usersLimit, null, 4);
				res.end(jsonText); 
            }
            else {
                res.sendStatus(400);
            }
        }
        else {
            res.sendStatus(400);
        }
    },

    getUserById(req, res) {
        const id = parseInt(req.params.id);
        const user = useRep.getUserById(id);
        if (user) {
            console.log(`Your request: ${req.metod} /api/users${req.url}`);
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/json');
            const jsonText = JSON.stringify(user, null, 4);
			res.end(jsonText);
        } 
        else {
            res.sendStatus(404);
        }
    }
};
