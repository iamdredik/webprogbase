const AnimeRepository = require('../repositories/animeRepository');
const animeRep = new AnimeRepository('./data/animes.json');
const Anime = require('../models/anime');
module.exports = {
    getAnimes(req, res) {
        console.log(`Your request: ${req.metod} /api/animes${req.url}`);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json');
        const animes = animeRep.getAnimes();
        if (req.query.limit) {
            const limit = parseFloat(req.query.limit);
            let arrLen = animes.length;
            let pages = 0;
            if (arrLen < limit) {
                res.sendStatus(404);
            }
            for (let i = arrLen; i > 0; i -= limit, ++pages) {}
            const page = parseInt(req.query.page);
            if (page <= pages) {
                const ind = (page - 1) * limit;
                let iter = 0;
                for (; iter < limit; iter++) {
                    if (ind + iter === arrLen) {
                        break;
                    }
                }
                const animesLimit = animes.slice(ind, ind + iter);
                const jsonText = JSON.stringify(animesLimit, null, 4);
				res.end(jsonText); 
            }
            else {
                res.sendStatus(400);
            }
        }
        else {
            res.sendStatus(400);
        }
    },

    getAnimeById(req, res) {
        const id = parseInt(req.params.id);
        const anime = animeRep.getAnimeById(id);
        if (anime) {
            console.log(`Your request: ${req.metod} /api/animes${req.url}`);
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/json');
            res.end(JSON.stringify(anime, null, 4));
        }
        else {
            res.sendStatus(404);
        }
    },

    postAnime(req, res) {
        console.log(`Your request: ${req.method} /api/animes${req.url}?options`);

        const anime = new Anime(0, req.body.title, req.body.studio, req.body.ongoing);
        anime.releaseDate = req.body.releaseDate ? Date.parse(req.body.releaseDate) : anime.releaseDate;
		anime.episodes = req.body.episodes ? parseInt(req.body.episodes) : anime.episodes;
        anime.id = animeRep.addAnime(anime).id;
        if (anime) {
            res.statusCode = 201;
            res.setHeader('Content-Type', 'text/json');
            const jsonText = JSON.stringify(anime, null, 4);
            res.end(jsonText);
        }
        else {
            res.sendStatus(400);
        }
    },

    putAnime(req, res) {
        console.log(`Your request: ${req.method} /api/animes${req.url}?options`);
		if(req.body.id) {
			const id = parseInt(req.body.id);
			const anime = animeRep.getAnimeById(id);
			if(anime) {
				anime.title = req.body.title ? req.body.title : anime.title;
                anime.studio = req.body.studio ? req.body.studio : anime.studio;
                anime.ongoing = req.body.ongoing ? req.body.ongoing : anime.ongoing;
                anime.releaseDate = req.body.releaseDate ? Date.parse(req.body.releaseDate) : anime.releaseDate;
		        anime.episodes = req.body.episodes ? parseInt(req.body.episodes) : anime.episodes;
				animeRep.updateAnime(anime);

				res.statusCode = 200;
                res.setHeader('Content-Type', 'text/json');
                const jsonText = JSON.stringify(anime, null, 4);
                res.end(jsonText);
			}
			else {
				res.sendStatus(404);
			}
		} 
		else {
			res.sendStatus(400);
		}
    },

    deleteAnime(req, res) {
        console.log(`Your request: ${req.method} /api/animes${req.url}`);

        const id = parseInt(req.params.id);
        const anime = animeRep.deleteAnime(id);
        if(anime)
		{
			res.statusCode = 200;
			res.setHeader('Content-Type', 'text/json');
            const jsonText = JSON.stringify(anime, null, 4);
            res.end(jsonText);
		}
		else 
		{
			res.sendStatus(404);
		}
    }
};