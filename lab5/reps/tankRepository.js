const JsonStorage = require("../jsonStorage");
const Tank = require("../models/tank");

class TankRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    addTank(newTank) {
        const items = this.storage.readItems();
        newTank.id = items.nextId;
        items.items.push(newTank);
        this.storage.writeItems(items);
        this.storage.incrementNextId();
        return newTank.id;
    }

    getTanks() {
        // const items = this.storage.readItems();
        return this.storage.readItems().items;
    }

    getTankById(id) {
        const items = this.storage.readItems();
        for (const item of items.items) {
            if (parseInt(item.id) === parseInt(id)) {
                return new Tank(item.id, item.name, item.nation, item.tier, item.damage, item.dateOfCreate);
            }
        }
        return null;
    }

    updateTank(updatedTank) {
        const items = this.storage.readItems();
        for (const item of items.items) {
            if (parseInt(item.id) === parseInt(updatedTank.id)) {
                item.name = updatedTank.name;
                item.nation = updatedTank.nation;
                item.tier = parseInt(updatedTank.tier);
                item.damage = parseInt(updatedTank.damage);
                item.dateOfCreate = updatedTank.dateOfCreate;
                this.storage.writeItems(items);
                return;
            }
        }
        console.log("Tank was not found!".red);
        return null;
    }

    deleteTank(id) {

        const items = this.storage.readItems();
        for (const temp of items.items) {
            if (parseInt(temp.id) === parseInt(id)) {
                items.items.splice(items.items.findIndex(a => a.id === parseInt(id)), 1);
                this.storage.writeItems(items);

                return 1;
            }
        }
        return null;
    }
};

module.exports = TankRepository;