const User = require('../models/user');
const JsonStorage = require('../jsonStorage');

class UserRepository {

    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getUsers() {
        return this.storage.readItems().items;
    }

    getUserById(id) {
        const items = this.storage.readItems();
        for (const item of items.items) {
            if (parseInt(item.id) === parseInt(id)) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }
};

module.exports = UserRepository;