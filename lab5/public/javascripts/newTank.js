async function laod(d) {
    document.getElementsByClassName("lds-spinner")[0].style.display = "inline-block";
    document.getElementById("subBtn").setAttribute("disabled", "disabled");
    document.getElementById("newForm").reset();
    await sendData(d);
    document.getElementsByClassName("lds-spinner")[0].style.display = "none";
    document.getElementById("subBtn").removeAttribute("disabled");

}
function sendData(d) {
    return new Promise((resolve, reject) => {

        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener('load', function (e) {
            const target = e.target;
            let t = new messageValue({
                id: JSON.parse(target.responseText).id,
                name: JSON.parse(target.responseText).name
            });
            ws.send(JSON.stringify(t));
            let newTr = "<tr><td><a href=/tanks/" + JSON.parse(target.responseText).id + ">" + JSON.parse(target.responseText).name + "</a></td><td>" + JSON.parse(target.responseText).dateOfCreate + "</td></tr>";

            document.querySelectorAll(".mini-list tbody")[0].insertAdjacentHTML('afterbegin', newTr);
            resolve();

        });
        xhttp.open("POST", "/api/tanks", true);
        xhttp.setRequestHeader("Content-type",
            "application/json");
        xhttp.send(JSON.stringify(d));
    });
}

// Access the form element...
const form = document.getElementById("newForm");


form.addEventListener("submit", async function (event) {
    event.preventDefault();
    const t = Array.from(document.querySelectorAll("#newForm input"));
    const d = {
        name: t[0].value,
        nation: t[1].value,
        tier: t[2].value,
        damage: t[3].value,
    };
    await laod(d);
});