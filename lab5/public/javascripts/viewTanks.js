let castAlert = document.getElementsByClassName("castAlert")[0];
if (castAlert) {
    castAlert.style.display = "block";
    setTimeout(() => {
        castAlert.style.opacity = "0";
    }, 3000);
    setTimeout(() => {
        castAlert.style.display = "none";
    }, 5000);
}

const lastP = document.getElementById("lastP");
const prevP = document.getElementById("prevP");
const nextP = document.getElementById("nextP");
const firstP = document.getElementById("firstP");

const search = document.getElementById("search");

const pages = document.getElementsByClassName("pages")[0];

let info = 0;
let tanks = 0;

let currPage = 0;
let maxPage = 0;
let offset = 0;
let searchText = "";


async function start() {
    await setup();
    setTanks();
}
start();

function getInfo() {
    return new Promise(function (resolve, reject) {
        setTimeout(async () => {

            const xhttp = new XMLHttpRequest();
            xhttp.addEventListener('load', function (e) {
                const target = e.target;
                info = JSON.parse(target.responseText).info;
                tanks = JSON.parse(target.responseText).tanks;
                resolve(info, tanks);
            });
            xhttp.open("GET", "/api/tanks?offset=" + offset + "&search=" + searchText, true);
            xhttp.setRequestHeader("Content-type",
                "application/json");
            xhttp.send();
        }, 10);
    });
}

async function setup() {
    document.getElementById("myModal").style.display = "block";
    document.getElementsByClassName("lds-spinner")[0].style.display = "inline-block";

    await getInfo();
    document.getElementById("myModal").style.display = "none";
    document.getElementsByClassName("lds-spinner")[0].style.display = "none";
    !info.lastActive ? lastP.classList.add("disable") : lastP.classList.remove("disable");
    !info.nextActive ? nextP.classList.add("disable") : nextP.classList.remove("disable");
    !info.prevActive ? prevP.classList.add("disable") : prevP.classList.remove("disable");
    !info.firstActive ? firstP.classList.add("disable") : firstP.classList.remove("disable");
    pages.innerText = info.currPage + "/" + info.maxPage;

}

function setTanks() {

    let html = '<li class="head">' +
        '<div class="head-list">' +
        '<div>' +
        '<p>name</p>' +
        '</div>' +
        '<div>' +
        '<p>nation</p>' +
        '</div>' +
        '<div>' +
        '<p>tier</p>' +
        '</div>' +
        '</div>' +
        '</li>';
    tanks.length <= 0 ?
        html += '<li class="notfound">' +
        '<h3>nothing found</h3>' +
        '</li>' :
        tanks.forEach(element => {
            html += '<li class="case">' +
                '<a href=/tanks/' + element.id + '>' +
                '<div class="case-body">' +
                '<div>' +
                '<p>' + element.name + '</p>' +
                '</div>' +
                '<div>' +
                '<p>' + element.nation + '</p>' +
                '</div>' +
                '<div>' +
                '<p>' + element.tier + '</p>' +
                '</div>' +
                '</div>' +
                '</a>' +
                '</li>' +
                '<hr />';
        });
    document.getElementsByClassName("entities-list")[0].innerHTML = html;
}

lastP.onclick = async function () {

    offset = info.last;
    await setup();
    setTanks();
};

prevP.onclick = async function () {
    offset -= 9;
    await setup();
    setTanks();
};

nextP.onclick = async function () {
    offset += 9;
    await setup();
    setTanks();
};

firstP.onclick = async function () {
    offset = info.first;
    await setup();
    setTanks();
};

search.onclick = async function () {
    searchText = document.getElementById("searchText").value;
    offset = 0;
    await setup();
    setTanks();
};