// Get the modal
let modal = document.getElementById("myModal");
let modal2 = document.getElementsByClassName("modal-content")[0];


// Get the button that opens the modal
let btn = document.getElementById("rmBtn");
let btnAcpt = document.getElementById("acptRm");
let btnC = document.getElementById("CB");

async function load(d) {
    document.getElementsByClassName("lds-spinner")[0].style.display = "inline-block";

    await removeTank(d);

    post('/tanks', d);
    setTimeout(() => {
        document.getElementsByClassName("lds-spinner")[0].style.display = "none";
        console.log("test");
    }, 4000);

}
// let form = document.getElementById('yesForm');
// form.addEventListener("submit", evt => {

//     evt.preventDefault();

//     setTimeout(() => {
//         form.submit();
//     }, 2000);
// });

function post(path, params, method = 'post') {

    // The rest of this code assumes you are not using a library.
    // It can be made less verbose if you use one.
    const form = document.createElement('form');
    form.method = method;
    form.action = path;

    for (const key in params) {
        if (params.hasOwnProperty(key)) {
            const hiddenField = document.createElement('input');
            hiddenField.type = 'hidden';
            hiddenField.name = key;
            hiddenField.value = params[key];

            form.appendChild(hiddenField);
        }
    }

    document.body.appendChild(form);
    form.submit();
}

async function removeTank(d) {
    return new Promise(function (resolve, reject) {
        const xhttp = new XMLHttpRequest();
        xhttp.addEventListener('load', function (e) {
            resolve();
        });
        xhttp.open("POST", "/tanks", true);
        xhttp.setRequestHeader("Content-type",
            "application/json");
        xhttp.send();
    });
}

btnAcpt.onclick = async function () {
    let d = {
        id: document.getElementById("id").textContent,
        name: document.getElementById("name").textContent
    };
    await load(d);
};

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
    modal2.style.display = "block";
};

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
    modal2.style.display = "none";
};
btnC.onclick = function () {
    modal.style.display = "none";
    modal2.style.display = "none";
};
// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target === modal) {
        modal.style.display = "none";
        modal2.style.display = "none";
    }
};