class messageValue {
    constructor(message) {
        this.timestamp = Date.now();
        this.message = message;
    }
}
const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
const wsLocation = `${protocol}//${location.host}`;
const ws = new WebSocket(wsLocation);
// function setStatus(value)
// {
//     status.innerHTML = value;
// }
function outputMsg(value) {
    let content = document.getElementsByClassName('notPlace');
    if (!content.length) {
        let place = document.createElement('div');
        place.classList.add("notPlace");
        document.getElementsByClassName("content")[0].insertAdjacentElement("afterend", place);
    } else {
        let j = 0;
        for (let i = content.length - 1; i >= 0; i--) {
            document.getElementsByClassName("notifications")[i].style.bottom = (j + 1) * 110 + 28 + 'px';
            j++;
        }
        let place = document.createElement('div');
        place.classList.add("notPlace");
        document.getElementsByClassName("notPlace")[content.length - 1].insertAdjacentElement("afterend", place);
    }
    content = document.getElementsByClassName('notPlace');
    let html = '<div class="notifications">' +
        '<span class="close" onclick="closeNot(' + (content.length - 1) + ')">&times;</span>' +
        '<a href="/tanks/' + JSON.parse(value).message.id + '">' +
        '<p>added tank at ' + (new Date(JSON.parse(value).timestamp).toLocaleTimeString()) + '</p>' +
        '<p>' + JSON.parse(value).message.name + '</p>' +
        '</a>' +
        '</div>';
    content[content.length - 1].innerHTML = html;
    let content2 = document.getElementsByClassName('notifications');
    content2[content2.length - 1].style.bottom = -200 + "px";
    setTimeout(() => {
        content2[content2.length - 1].style.bottom = 28 + "px";
    }, 10);


    if (content.length === 5) {
        content2[0].style.opacity = '0';
        setTimeout(() => {
            content[0].remove();
        }, 400);
    }
}

async function closeNot(i) {
    let content = document.getElementsByClassName('notPlace');
    let content2 = document.getElementsByClassName('notifications');
    content2[i].style.opacity = '0';
    setTimeout(() => {
        content[i].remove();
        content = document.getElementsByClassName('notPlace');
        let j = 0;
        for (let i = content.length - 1; i >= 0; i--) {
            document.getElementsByClassName("notifications")[i].style.bottom = j * 110 + 28 + 'px';
            j++;
            document.getElementsByClassName("close")[i].setAttribute('onclick', 'closeNot(' + i + ')');
        }
    }, 400);

};

ws.onopen = () => {
    console.log("CONNECTED");
};
ws.onclose = () => {
    console.log("DISCONNECTED");
};
ws.onmessage = response => outputMsg(response.data);