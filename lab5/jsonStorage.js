const fs = require('fs');

class JsonStorage {

    // filePath - path to JSON file
    constructor(filepath) {
        this.filepath = filepath;
    }

    get nextId() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray.nextId;
    }

    incrementNextId() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        jsonArray.nextId++;
        this.writeItems(jsonArray);
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filepath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    writeItems(items) {
        // try {
            // const object = JSON.parse(items);
            const temp = JSON.stringify(items, null, '\t');
            fs.writeFileSync(this.filepath, temp,);
        // }
        // catch
        // {
        //     console.log("Caught an error: " + err.message);
        //     // Caught an error: SyntaxError: Unexpected end of JSON input
        // }
    }
};

module.exports = JsonStorage;