//

const tankController = require('./../controllers/tanks');
const tankRouter = require('express').Router();


tankRouter
    .get('/', tankController.getInfo, tankController.getTanks)
    .get('/:id(\\d+)', tankController.searchTankById, tankController.getTankById)
    .post('/', tankController.addTank)
    .put('/', tankController.updataTank)
    .delete('/:id(\\d+)', tankController.searchTankById, tankController.deleteTankById);

module.exports = tankRouter;