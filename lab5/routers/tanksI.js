//

const tankController = require('./../controllers/tanks');
const tankRouter = require('express').Router();

tankRouter
    .get('/tanks', tankController.viewTanks)
    .get('/tanks/new', tankController.addPage)
    .get('/tanks/:id', tankController.viewTank)
    .post('/tanks', tankController.deleteTankById, tankController.viewTanks)

    .post('/tanks/new', tankController.addTank);

module.exports = tankRouter;