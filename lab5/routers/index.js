const express = require('express');
const router = express.Router();
const indexController = require('../controllers/index');
const tanksController = require('../controllers/tanks');
const tankRouter = require('./tanksI');

router.get('/about', indexController.getAbout);
router.get('/', indexController.getHome);
router.use('', tankRouter);
router.get('/favicon.ico', () => {

});
router.get('/info', tanksController.getInfo);





module.exports = router;