//

const mediaController = require('./../controllers/media');
const mediaRouter = require('express').Router();

/**
 * @route POST /api/media
 * @group Media - media operations
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 201 - added image id
 */

/**
 * @route GET /api/media/{media_id}
 * @group Media - media operations
 * @param {integer} media_id.path.required - id of the Media - eg: 1
 * @returns {Media.model} 200 - media object
 * @returns {Error} 404 - media not found
 */

mediaRouter
    .post('/', mediaController.addMedia)
    .get('/:media_id(\\d+)', mediaController.searchMediaById, mediaController.getMediaById);

module.exports = mediaRouter;