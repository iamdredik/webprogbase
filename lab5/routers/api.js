const userRouter = require('./users');
const tankRouter = require('./tanks');
const mediaRouter = require('./media');
const router = require('express').Router();

router.use('/users', userRouter);
router.use('/tanks', tankRouter);
router.use('/media', mediaRouter);

module.exports = router;