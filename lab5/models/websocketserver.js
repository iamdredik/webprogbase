const WebSocketServer = require('ws').Server;
class WsServer {
    constructor(server) {
        this.connections = [];
        this.wsServer = new WebSocketServer({
            server: server
        });
        this.wsServer.on('connection', (connection) => {
            connection.on('message', (message) => {
                this.wsServer.clients.forEach(client => {
                    client.send(message);
                });
            });
            connection.on('close', () => this.removeConnection(connection));
        });
    }

    addConnection(connection) {
        this.connections.push(connection);
    }
    removeConnection(connection) {
        this.connections = this.connections.filter(x => x !== connection);
    }
};


module.exports = WsServer;