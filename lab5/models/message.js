class Message {
    constructor(message) {
        this.timestamp = Date.now();
        this.message = message;
    }
}
module.exports = Message;