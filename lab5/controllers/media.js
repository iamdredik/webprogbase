//

const Media = require('./../models/media');
const mediaRepository = require('./../reps/mediaRepository');
const storage = new mediaRepository('./data/media.json');

const searchMedia = Symbol('media');

module.exports = {

    addMedia(req, res) {
        console.log(`Request: ${req.method} /api/media${req.url}`);
        
        const media = new Media(1, req.files['file-key'].name, req.files['file-key'].encoding, req.files['file-key'].mimetype, req.files['file-key'].truncated, "", req.files['file-key'].size);
        const idFile = storage.addMedia(media, req.files['file-key'].data);

        res.statusCode = 201;
        res.setHeader('Content-Type', 'text/json');
        const status = JSON.stringify({
            status: 'ok',
            id: idFile
        }, null, 4);
        res.end(status);
    },

    getMediaById(req, res) {
        console.log(`Request: ${req.method} /api/media${req.url}`);
        res.statusCode = 200;
        res.download(req[searchMedia].url);
    },

    searchMediaById(req, res, next) {
        const idMedia = parseInt(req.params.media_id);
        const media = storage.getMediaById(idMedia);
        if (media) {
            req[searchMedia] = media;
            next();
        } else {
            res.sendStatus(404);
        }
    }

};