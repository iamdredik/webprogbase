const userRepository = require('./../reps/userRepository');
const storage = new userRepository('./data/users.json');

const searchUser = Symbol('user');

module.exports = {

    getUsers(req, res) {
        console.log(`Request: ${req.method} /api/users${req.url}`);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json');
        const users = storage.getUsers();

        if (req.query.limit && req.query.offset) {
            const limit = parseInt(req.query.limit);
            const offset = parseInt(req.query.offset);
            if (limit < 0 || offset < 0) {
                res.statusCode(400);
            } else if (users.length <= offset) {
                res.end('[]');
            } else {
                const usersLimit = users.slice(offset, offset + limit);
                const usersOut = JSON.stringify(usersLimit, null, 4);
                res.end(usersOut);
            }
        } else {
            const usersOut = JSON.stringify(storage.getUsers(), null, 4);
            res.end(usersOut);
        }
    },

    getUserById(req, res) {
        console.log(`Request: ${req.method} /api/users${req.url}`);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json');
        const users = JSON.stringify(req[searchUser], null, 4);
        res.end(users);
    },

    searchUserById(req, res, next) {
        const idUser = parseInt(req.params.id);
        const user = storage.getUserById(idUser);
        if (user) {
            req[searchUser] = user;
            next();
        } else {
            res.sendStatus(404);
        }
    }

};