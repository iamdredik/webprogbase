const musicRepository = require('./../reps/tankRepository');
const storage = new musicRepository('./data/tanks.json');
const Tank = require('./../models/tank');

const searchTrack = Symbol('tank');

module.exports = {

    getTanks(req, res) {

        const tanks = storage.getTanks();
        const limit = 9;
        if (req.query.search) {
            const search = req.query.search;
            for (let i = 0; i < tanks.length;) {
                if (!tanks[i].name.includes(search)) {
                    tanks.splice(i, 1);

                } else {
                    i++;
                }
            }
        }

        if (limit && req.query.offset) {

            const offset = parseInt(req.query.offset);
            if (limit < 0 || offset < 0) {
                res.statusCode(400);
            } else {
                const tanksLimit = tanks.slice(offset, offset + limit);
                const tanksOut = JSON.stringify({
                    tanks: tanksLimit,
                    info: res.pInfo
                }, null, 4);
                res.end(tanksOut);
            }
        } else {
            const tanksOut = JSON.stringify({
                tanks: storage.getTanks(),
                info: res.pInfo
            }, null, 4);

            res.end(tanksOut);
        }

    },

    getTankById(req, res) {

        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/json');
        const trackOut = JSON.stringify(req[searchTrack], null, 4);
        res.end(trackOut);
    },

    addTank(req, res) {


        const tank = new Tank(null, null, null, null, null, null);
        // 
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        // 
        tank.dateOfCreate = req.body.dateOfCreate ? Date.parse(req.body.dateOfCreate) : today;
        tank.damage = req.body.damage ? parseFloat(req.body.damage) : tank.damage;
        tank.tier = req.body.tier ? parseFloat(req.body.tier) : tank.tier;
        tank.nation = req.body.nation;
        tank.name = req.body.name;
        tank.id = storage.addTank(tank);

        res.statusCode = 201;
        res.setHeader('Content-Type', 'text/json');
        const trackOut = JSON.stringify(tank, null, 4);
        res.end(trackOut);

    },

    updataTank(req, res) {
        if (req.body.id) {
            const tankId = parseInt(req.body.id);
            const tank = storage.getTankById(tankId);
            if (tank) {
                tank.name = req.body.name ? req.body.name : tank.name;
                tank.nation = req.body.nation ? req.body.nation : tank.nation;
                // 
                let today = new Date();
                let dd = String(today.getDate()).padStart(2, '0');
                let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
                let yyyy = today.getFullYear();

                today = mm + '/' + dd + '/' + yyyy;
                // 
                tank.dateOfCreate = req.body.dateOfCreate ? Date.parse(req.body.dateOfCreate) : today;
                tank.tier = req.body.tier ? parseFloat(req.body.tier) : tank.tier;
                tank.damage = req.body.damage ? parseFloat(req.body.damage) : tank.damage;
                storage.updateTank(tank);

                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/json');
                const tankOut = JSON.stringify(tank, null, 4);
                res.end(tankOut);
            } else {
                res.sendStatus(404);
            }
        } else {
            res.sendStatus(400);
        }
    },

    deleteTankById(req, res, next) {
        storage.deleteTank(req.body.id);
        next();
    },

    searchTankById(req, res, next) {
        const tankId = parseInt(req.params.id);
        const tank = storage.getTankById(tankId);
        if (tank) {
            req[searchTrack] = tank;
            next();
        } else {
            res.sendStatus(404);
        }
    },

    checkParameters(req, res, next) {
        if (req.body.nation && req.body.name) {
            next();
        } else {
            res.sendStatus(400);
        };
    },
    async addPage(req, res) {
        const tank = storage.getTanks().reverse();
        res.render('new', {
            entities: tank,

        });
    },
    viewTanks(req, res) {
        // const limit = 9;
        // const tank = storage.getTanks();
        // if (req.query.search) {
        //     const search = req.query.search;

        //     for (let i = 0; i < tank.length;) {
        //         if (!tank[i].name.includes(search)) {
        //             tank.splice(i, 1);

        //         } else {
        //             i++;
        //         }
        //     }
        // }

        // const offset = parseInt(req.query.offset) || 0;
        // if (limit < 0 || offset < 0) {} else if (tank.length <= offset) {
        //     res.render('tanks', {
        //         tanks: true,
        //         entities: 0,
        //         pagination: getInfo(req, res)

        //     });
        // } else {
        //     const tanksLimit = tank.slice(offset, offset + limit);
        let removed = null;
        if (req.body.id) {
            removed = {
                id: req.body.id,
                name: req.body.name
            };
        }
        res.render('tanks', {
            tanks: true,
            // entities: tanksLimit,
            pagination: getInfo(req, res),
            removed: removed

        });
        // }

    },
    viewTank(req, res) {
        const tank = storage.getTankById(req.params.id);
        res.render('tank', {
            tank: tank,
        });
    },
    getInfo(req, res, next) {
        res.pInfo = getInfo(req, res);
        next();
    }

};

function getInfo(req, res) {
    const p = {
        first: 0,
        last: 0,
        prev: 0,
        next: 0,
        firstActive: true,
        lastActive: true,
        prevActive: true,
        nextActive: true,
        search: "",
        notfound: false,
        ifsearch: false,
        currPage: 1,
        maxPage: 1
    };
    let offset = req.query.offset || req.body.offset;
    const tanks = storage.getTanks();
    if (req.query.search) {
        p.ifsearch = true;
        p.search = req.query.search;
        const search = req.query.search;
        for (let i = 0; i < tanks.length;) {
            if (!tanks[i].name.includes(search)) {
                tanks.splice(i, 1);

            } else {
                i++;
            }
        }
    }
    p.maxPage = Math.ceil(tanks.length / 9);
    p.currPage = parseInt(offset) / 9 + 1;
    p.last = (Math.ceil(tanks.length / 9) - 1) * 9;
    if (parseInt(offset) === p.last || p.last < 0) {
        p.lastActive = false;
        p.nextActive = false;
    }
    if (p.last < 0) {
        p.notfound = true;
        p.maxPage = 1;
        p.currPage = 1;
    }
    if (parseInt(offset) === 0) {
        p.prevActive = false;
        p.firstActive = false;
    }
    p.next = parseInt(offset) + 9;
    p.prev = (Math.ceil(parseInt(offset) / 9) - 1) * 9;

    return p;
}